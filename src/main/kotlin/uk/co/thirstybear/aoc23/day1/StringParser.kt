package uk.co.thirstybear.aoc23.day1

import java.io.File

fun main() {
    print(StringParser.accumulator(File("myfile.txt").readLines()))
}

class StringParser {
    companion object {
        private val numericChars = '0'..'9'

        private val word2numbers = mapOf(
            "one" to "1",
            "two" to "2",
            "three" to "3",
            "four" to "4",
            "five" to "5",
            "six" to "6",
            "seven" to "7",
            "eight" to "8",
            "nine" to "9",
            "zero" to "0"
        )

        fun parse(s: String): Int {
            val firstInteger = findFirstDigit(s)
            val lastInteger = findLastDigit(s)

            return "$firstInteger$lastInteger".toInt()
        }

        fun accumulator(strings: List<String>): Int {
            return strings.fold(0) { acc, next -> acc + parse(next) }
        }

        private fun findFirstDigit(s:String): String? {
            if (s.first() in numericChars) return s.first().toString()
            word2numbers.forEach { (key, value) ->
                if (s.startsWith(key)) return value
            }
            return findFirstDigit(s.drop(1))
        }

        private fun findLastDigit(s:String): String? {
            if (s.last() in numericChars) return s.last().toString()
            word2numbers.forEach { (key, value) ->
                if(s.endsWith(key)) return value
            }
            return findLastDigit(s.dropLast(1))
        }
    }

}