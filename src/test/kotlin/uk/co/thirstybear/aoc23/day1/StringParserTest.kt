package uk.co.thirstybear.aoc23.day1

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class StringParserTest {
    @Test
    fun combinesFirstAndLastDigit() {
        assertEquals(12, StringParser.parse("1abc2"))
        assertEquals(38, StringParser.parse("pqr3stu8vwx"))
        assertEquals(15, StringParser.parse("a1b2c3d4e5f"))
        assertEquals(77, StringParser.parse("treb7uchet"))

    }

    @Test
    fun interpretsWordsToDigits() {
        assertEquals(29, StringParser.parse("two1nine"))
        assertEquals(83, StringParser.parse("eightwothree"))
        assertEquals(13, StringParser.parse("abcone2threexyz"))
        assertEquals(24, StringParser.parse("xtwone3four"))
        assertEquals(42, StringParser.parse("4nineeightseven2"))
        assertEquals(14, StringParser.parse("zoneight234"))
        assertEquals(76, StringParser.parse("7pqrstsixteen"))
    }

    @Test
    fun accumulatesValue() {
        assertEquals(142, StringParser.accumulator(listOf("1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet")))
    }

}